# Continuous Delivery in the Cloud

 
<div style="float: right; padding-right: 1em;"><img height="180px";  src="baktapur.png" /></div>
*Continuous delivery has become one of the most popular software patterns in recent years. It changes much of the way we are thinking about software. There is no plan for the release next may. There are features that will be in production tomorrow, or at least in a week. Internet companies like flickr or amazon are proud to delivery new features constantly (e.g. amazon claims to release a new feature every 11.6 seconds). Let's see how cloud computing is a perfect fit for implementing continuous delivery.*

## Deliver new features on mouse-click
Although most products do not have to deliver many times a day, it is a benefit for each and every project to be able to deliver a new feature with a single mouse click. This enables us not only to show our customers the fix for the bug discussed yesterday, but it helps us to keep our software development, configuration management, and release management processes simple and focused. 

## A simple example
The target environment of our example system is quite simple, but common. There is the web server, a database, and a filestore. All operated by amazon web services (AWS), the web server as an ec2 instance, the database as an MySQL RDS system, and the file store as a S3 bucket. 

<div style="padding: 1em;"><img height="300" src="arch.png" /></div>

As a build system we use Atlassian on Demand Bamboo which provides a pay-as-you-go model. Bamboo uses elastic agents running on the AWS cloud to run the build scripts.

After compilation and unit testing, bamboo deploys our software directly to the target environment. 

Deployment does not only include web server deployment (e.g. deploying a war-file to the web server), but also database updates. A migration tool helps us to run incrementally update our database. In our example, we use [MyBatis migrations](http://www.mybatis.org/migrations "MyBatis migrations"), where the database can be updated with a simple `migration up`.

Last but not least, integration web testing happens after deployment which ensures that the  application is available and no existing functionality has been broken (e.g. using [Selenium](http://seleniumhq.org "Selenium")). 

## One click production deployment

Whereas we deploy automatically and continuously after each check in to the development environment, we use a humane gatekeeper for the production environment. Only when new features are tested in an explorative way on the development environment, the production system is deployed. A deployment that happens with a single mouse-click, of course.

## Perfect fit
Cloud is a perfect fit for implementing continuous delivery, because it allows not only to easily automate the delivery of software but also to automate the delivery of virtual hardware, too. 

