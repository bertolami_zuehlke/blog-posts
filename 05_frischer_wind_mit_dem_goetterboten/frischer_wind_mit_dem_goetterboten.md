# Frischer Wind mit dem neuen Götterboten 

<div style="float: left; padding-right: 1em;"><img width="220px";  src="zuckerstock.jpg" /></div>
*Am 22. April fand im Kursaal Bern die Vernissage zur neusten Version der Projektmanagement-Methode HERMES statt. HERMES 5 und das von Zühlke entwickelte Anwenderwerkzeug stiessen bei den zahlreich erschienenen Teilnehmern auf grosses Interesse.*

## Vernissage
Das Informatiksteuerungsorgan des Bundes (ISB) lud zur Vernissage der neuen Version von HERMES in den Berner Kursaal ein. Die Veranstaltung war schon bald ausverkauft. Über 400 Teilnehmer wollten es sich nicht entgehen lassen, sich direkt von den Neuheiten zu überzeugen. 

[Fotogallerie zur Vernissage](http://www.egovernment.ch/dokumente/publikationen/hermes5_startseite/hermes5_vernissage/index.html)

## Methode

Die neue Version von HERMES wurde in kurzen Vorträgen aus verschiedenen Blickwinkeln beleuchtet. Nicht nur die Methode selber, Zertifizierungen und die Buchpublikation wurden diskutiert, sondern auch Revisionsaspekte und die Frage, wie IT-Projekte im Sinne der Nachhaltigkeit durchgeführt werden können.

Um den Einstieg in HERMES 5 zu erleichtern wurde sogar ein kurzes Einführungsvideo gedreht. <a href="http://www.youtube.com/watch?v=f2FftcmrhwI" target="_new">Film ab</a>.


## Anwenderwerkzeug

Zühlke hat für das ISB das Online Werkzeug zu HERMES 5 entwickelt und durfte dieses an der Vernissage auch gleich selber präsentieren. Das Interesse war gross, die Rückmeldungen sehr positiv. 

<div><img width="420px";  src="tool_presentation.png" /></div>

Insbesondere das Zurechtstutzen und das einfache Erweitern von vordefinierten Szenarien wurde geschätzt, denn für ein konkretes Projekt ist selten bis nie die gesamte Methode relevant.

Ausserdem waren viele Teilnehmer interessiert an den Erfahrungen, die Zühlke mit dem Einsatz von Cloud Computing für Entwicklung und Betrieb des Werkzeuges gemacht hat.

Wir laden deshalb alle ein, sich selbst ein Bild zu machen von frischen Wind des neuen Götterboten und dem Anwenderwerkzeug unter  [http//www.hermes.admin.ch](http//www.hermes.admin.ch).

 