# CAS Cloud Computing 2015 

Bereits zum vierten Mal wird an der [Berner Fachhochschule](http://www.ti.bfh.ch/index.php?id=6369) das CAS zum Thema Cloud Computing durchgeführt. Wie in den letzten Jahre, übernimmt Zühlke die Module "IaaS-Dienste und ICT Grundlagen", "Paas-Dienste, Software-Schnittstellen und Frameworks" und "Applikationstransformation für die Cloud". Das Zühlke Dozententeam besteht aus Christian Tschenett, Markus Leder, Roman Bertolami und neu Stefan Jäger. Von seiner grossen Erfahrung im Bereich PaaS, NoSQL und Cloud Migrationen profitieren alle Teilnehmer. 

### Deutliche Teilnehmerzunahme

Erfreulich ist die deutliche Teilnehmerzunahme im Vergleich zu den Vorjahren. Die Ränge sind voll, das Thema Cloud Computing nun definitiv am Markt angekommen. Das Publikum besteht aus kritischen und diskussionsfreudigen Zuhörern. Dadurch entstehen immer wieder spannende Auseinandersetzungen. Wie immer in den letzten Jahren ist das Publikum sehr heterogen. Dank dieser Diversität lernen die Teilnehmer, wie das Thema Cloud Computing aus verschiedensten Blickwinkeln wahrgenommen wird. 

### Private Cloud - Public Cloud

Einzelne Teilnehmer bauen bei ihrem jeweiligen Arbeitgeber eine Private Cloud auf. Im Kurs erfahren sie, welche Dienstleistungen verschiedene Public Cloud Provider anbieten. Dadurch sind sie in der Lage die eigene Private Cloud einzuordnen und allfällige Anpassungen vorzunehmen. Gross ist das Interesse an Windows Azure, da Microsoft mit dem Windows Azure Pack eine Cloud Platform zur Installation im eigenen Rechenzentrum zur Verfügung stellt.

### Moderne Cloud Architekturen

Moderne, cloudfähige Architekturen mit Single Page Applications, Mobile Clients und REST Backend sind Themen, die im PaaS Modul intensiv diskutiert werden. In den Live-Demos wird schnell ersichtlich was Vendor Lock-In konkret bedeutet. Die Teilnehmer zeigen auch viel Interesse an den Grundlagen im Bereich NoSQL und Map Reduce und diskutierten, wie sich diese Technologien aktuell weiterentwickeln.

### Unzählige PaaS Anbieter 

Während es vor ein paar Jahren noch möglich war, ein Anbieterübersicht über die aktuell relevanten PaaS Anbieter zu machen ist dies heute de facto nicht mehr möglich. Es gibt mittlerweile hunderte von verschiedenen PaaS Anbieter. Ein weiteres Indiz, dass Cloud Computing am Markt weit verbreitet ist. Was allerdings noch fehlt ist eine Standardisierung, was es schwierig macht, die Angebote zu vergleichen. Auch weitgehend unbeantwortet bleibt die Frage, wie eine konkrete private PaaS Umgebung für den eigenen Arbeitgeber aussehen würde.

### Intensive Gruppenarbeit
Im Modul Applikationstransformation für die Cloud wird anhand eines realistischen Fallbeispiels das bisher erworbene Wissen angewendet. In der intensiven Gruppenarbeit entwerfen die Teilnehmer erfolgsversprechende Lösungskonzepte wie eine Individualsoftware in die Cloud migriert werden kann.  

Die Durchführung des CAS Cloud Computing 2015 hat richtig Spass gemacht und wir freuen uns schon auf die nächste Runde im 2016.

Weitere Blog Posts zum CAS Cloud Computing:

[Erfahrungsbericht CAS Cloud Computing 2013](http://blog.zuehlke.com/cas-cloud-2013/)

[CAS Cloud Computing an der Berner Fachhochschule mit Zühlke](http://blog.zuehlke.com/cas-cloud-computing-an-der-berner-fachhochschule-mit-zuhlke/)