Was ist das Wichtigste, was das Meetup bietet?

- Austausch und Vernetzung von Interessierten der Region Bern

- Zielgruppe sind Praktiker, welche im Themengebiet arbeiten oder einfach Interesse haben

- Kurze Präsentationen, welche anschliessend die Diskussion beim Apéro fördern

 

Warum kommen die Teilnehmer wieder?

- Kurze Präsentationen

- Apéro

- Austausch in ungezwungener Atmosphäre

- Keine abgehobenen, akademisierten Diskussionen


 

Wo steht das Meetup in 1-2 Jahren?

- Da müssten wir uns mal Gedanken machen :) 



Weitere Inputs?

- Unbedingt Fotos machen, das nächste Mal sind viele Leute angemeldet