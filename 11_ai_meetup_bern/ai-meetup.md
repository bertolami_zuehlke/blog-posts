# Machine Learning & Artificial Intelligence Meetup Bern

Bereits zum dritten Mal wurde bei Zühlke Bern das Machine Learning & Artificial Intelligence Meetup Bern durchgeführt. Gemeinsam mit dsi engineering ag hat Zühlke dieses Jahr den Austausch rund um Künstliche Intelligenz in Region Bern ins Leben gerufen.


### Wie alles begann

Im Frühjahr 2018 beschlossen ein paar Zühlke Mitarbeiter, dass sie den Austausch im Bereich künstlicher Intelligenz und Machine Learning am Standort Bern fördern möchten. Zuerst war geplant etwas firmenintern auf die Beine zu stellen. Die Idee einer öffentlichen Veranstaltung überzeugte aber alle. Die Möglichkeit, den Austausch über mehrere Firmen und Institutionen hinweg zu führen klang spannend.

Um nicht als tolle Idee in der Versenkung haben wir, bevor wir inhaltlich genau wussten was passieren soll, einen Termin gesucht und auch gleich auf meetup.com publiziert. Mit wachsenden Teilnehmerliste stieg die Motivation, die Vision der Veranstaltung zu formen und erste Referenten zu suchen.

### Vision

### Was bisher geschah

### Stimmen der Teilnehmer


[Link zur Meetup Gruppe](http://meetu.ps/e/Frl31/zXhCf/f)

[Link zu den publizierten Unterlagen](https://github.com/mlbe)