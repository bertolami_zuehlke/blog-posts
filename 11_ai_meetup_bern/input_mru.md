Was ist das Wichtigste, was das Meetup bietet?
Austausch zwischen Ai Enthusiasten und Practitioners im Raum Bern

Warum kommen die Teilnehmer wieder?
In kurzer Zeit das Problem und die ein Lösungsansatz erklärt. Austausch, Networking und Inspiration mit anderen Teilnehmer.

Wo steht das Meetup in 1-2 Jahren?
In zwei Jahren sind wir wohl im Tal der Tränen (Gartner) und befassen uns mit Themen wie Wartbarkeit, Testbarkeit, Entwicklungsprozesse, AI as Service und weniger über Algorithmen und Deep Learning Architekturen.

Oder (Achtung Humor): Es braucht das Meetup nicht mehr, da die Roboter (SKYNET)  die Menschheit bereits versklavt haben und das Meetup somit obsolet ist.