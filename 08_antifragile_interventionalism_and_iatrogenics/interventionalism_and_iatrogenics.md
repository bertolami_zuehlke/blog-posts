# Antifragile Software Development - Part 3: Interventionism  and Iatrogenics
<div style="float: left; padding-right: 1em;"><img width="220px"; src="redemptor.jpg" /></div>

*In his seminal book "Antifragile - Things that gain from disorder" N.N. Taleb makes some bold statements derived from a risk based view of the world. In this series we want to look at some his claims from a software development perspective. The third part covers iatrogenics (preventable harm resulting from treatment or advice)*

"The first principle of iatrogenics is as follows: we do not need evidence of harm to claim that a drug or an unnatural via positiva procedure is dangerous."

"Second principle of iatrogenics: it is not linear. We should not take risks with near-healthy people; but we should take a lot, a lot more risks with those deemed in danger."


## Try hard to remove any feature you can

"Anything in which there is interventionism will have iatrogenics."

A common way of interventionism in software engineering is to add all possibly useful feature to a product. The harm caused by unused or unnecessary features is not only the additional effort spent to implement the features in the first place, but the additional complexity introduced to the system which may cause maintenance cost that are highly non linear. 

If we try hard to avoid or remove any feature we can, which itself might be a costly process in the first place, we can keep the size and complexity of our system in relation to the problem we solve. We avoid nonlinear effects that arise from the problem with size.

A common way to achieve this goal is to develop software according to the [agile principles](http://www.agilemanifesto.org/principles.html) especially to "Simplicity - the art of maximizing the amount of work not done - is essential." Instead of specifying every possible feature upfront we start with a minimal product and adapt this product when additional needs occur. 

The importance of removing features is stressed by a statistics published by The Standish Group: 45% of an application's features are never used, 19% are rarely used, 16% are sometimes used, 13% are often used, and finally, 7% are always used. When we set this in relation to the [problem with size](problem_with_size.html) we can expect project and maintainance cost to fall dramatically without any impact on customer experience if we are able to reduce the number of features substantially.


## KISS & YAGNI

Not only features may be a source for interventionism, but also optmisations in the name of the so-called internal quality may cause harm. Overgeneralisation and introduction of unnecessary abstractions can significantly increase the complexity of a piece of software. 

"If it ain't broke don't fix is" is a good guideline to avoid unnecessary refactorings and re-designs which all come at a certain risk. 

Additinally, it is important to have peer reviews and critical colleagues that insist on the [KISS](http://en.wikipedia.org/wiki/KISS_Principle) (Keep it simple, stupid) and the [YAGNI](http://c2.com/cgi/wiki?YouArentGonnaNeedIt) (You Arent Gonna Need It) principles. The YAGNI principle is especially useful since it prevents us from introducing features and code that are neither required nor bring any benefit but only increase complexity and maintainability cost in a non-linear way.


## The myth of re-use and standardization

A common source for unneccessary complexity is the desire to introduce standardization. Following the standards is indeed a good way to increase maintainability. However, it is crucial to allow different kinds of solutions to different kinds of problems, in order to keep the solutions clean and without additional complexity. The desire to introduce standardization often tries to solve all kind of problems with one kind of solution. The solution itselfs becomes more complex and less adapted to the actual problem it solves.

A similar problem arises when we try to re-use software components. While this works well for low level components, e.g. a string utility component, it becomes expensive for higher level components. The cost of building a re-usable higher level component is X times the cost of building a specific component for a specific problem.

## Identify interventionism
Being aware of interventionism and iatrogenics in our everyday work can substantionally help to build better software with less but more usefull features. Questions like 'is there no simpler solution?' or 'do we really need this?' can help to identify interventionism.
