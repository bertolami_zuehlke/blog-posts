# Antifragile Software Development - Part 1: The Problem with Size

<div style="float: left; padding-right: 1em;"><img width="220px";  src="itaipu.jpg" /></div>

*In his seminal book "Antifragile - Things that gain from disorder" N.N. Taleb makes some bold statements derived from a risk based view of the world. In this series we want to look at some his claims from a software development perspective. The first part covers the problem with size.*


"In spite of what is studied in business schools concerning 'economies of scale,' size hurts you at times of stress"

"The gains from size are visible but the risks are hidden, and some concealed risks seem to bring frailties into the companies."

According to Mr. Taleb "the large is doomed to breaking", an effect he describes for corporations, mammals and administration. 

An effect we are dealing with in different ways in software development, too, especially with large projects and big teams.   

## Large projects

"No psychologist who has discussed the 'planning fallacy' has realized that, at the core, it is not essentially a psychological problem, not an issue with human errors; it is inherent to the nonlinear structure of the projects."

When we estimate the cost of a software project we estimate the backlog items and assign them with minimal, most likely and maximal cost. However, we are unable to estimate the non-linear effects that may occur by unexpected changes or problems occurring during the project. The problem with large projects is, that the matter gets more and more complex and the non-linear interdepencencies have a substantially larger impact on cost and duration.

Thus, when the project size increases the risk of underestimating increases non-linearly, although we typically only have a linear XY% safety net. An illustration of the relationship of cost in dependence of size is given in the following figure.


<div style="padding-right: 1em;"><img width="420px";  src="problem_with_size.svg" /></div>
*Non-linear relationship of size to cost.

## Big Teams
Large projects require big teams with lots of people. We build multiple Scrum Teams, conduct daily Scrum of Scrums, etc. Again, the cost is not linear in relation to the generated value. The communication overhead increases dramatically. In a scrum master course I once attended I learned that you need more than three scrum teams to double the performance of a single team. 

## Modular systems
Of course we have some strategies to mitigate the problem with size. One of the most important strategy is to turn a large and dangerous "stone into a tousand pebbles". We split projects into subprojects and assure that the interface between the projects and systems are clearly defined. We try to build modular systems and carefully manage dependencies.  Managing dependencies means reducing dependencies whenever possible, especially if the benefit resulting from the dependency is small. 

## Try to keep your system small
The most effective way is to keep the system as small as possible to avoid negative non linear effects. Every features should be carefully examined whether it is really needed. Especially when considering that for every dollar spent for implementing a feature about nine dollars will have to be paid in the future for maintenance.

## Fail fast
It is essential in larger projects to run a risk-driven approach. Thus, we try to solve those problems first, that might be difficult or impossible to solve at all. This allows us to fail fast when we detect, that one of those problems cannot be solved or might result in much higher cost than expected.