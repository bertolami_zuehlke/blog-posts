# set title 'Relationships of Size and Cost'

# set output 'problem_with_size.svg'
# set terminal svg

set output 'problem_with_size.png'
set terminal png


set xlabel 'size'

set ylabel 'cost'

unset xtics
set xtics format ""
unset ytics
set ytics format ""



set xtic scale 0
set ytic scale 0

set xlabel font  "arial,14"
set ylabel font  "arial,14"

set xrange [0:2]

set key font "arial,14"
set key spacing 1.4
set key left top

plot x title "estimate" lw 3,  1.3*x title "safety net" lw 3,   0.8*x title "best case" lw 3 lt 4, exp(x) - 1  title "worst case" lw 3  lt 3