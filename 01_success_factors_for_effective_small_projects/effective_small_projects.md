# Efficiently Master Small Software Projects

*After having successfully conducted small software projects we want to share some experience and lessons learnt. Not surprising the success depends on people, process, and technology.*

<div style="float: right; padding-right: 1em;"><img height="250px";  src="hongkong39.jpg" /></div> 
Many of us working in software engineering business are working on projects that are big, difficult and complex. We work for big companies supporting important business. And we are dreaming of a small simple project that can be done without politics, management overhead and old fashioned technology. When we are finally asked to conduct such a small simple project we start smiling. At a closer look, typically one or two days later, we notice two things. First, the amount of money and time to conduct the dream is quite small. Second, we never did such a thing before, except that open-source project five years ago that finally didn't even made it [from SourceForge to GitHub](http://readwrite.com/2011/06/02/github-has-passed-sourceforge).

## People
For a small project your software development team needs to be small, typically about two developers. You will not benefit from more people involved, and if there is just one developer the [truck factor](http://en.wikipedia.org/wiki/Bus_factor) is scarily small. 
Furthermore, it is extremely important to have all skills required to master the project within your small team. 
Assure that you have a common understanding of software engineering among the team members, there is just no time for any [vi vs. emacs](http://en.wikipedia.org/wiki/Editor_war) discussions. 

Similar to a common understanding among the team members is a clear vision shared with the customer.
A key success factor for small projects is a pragmatic customer that is open to compromises and shortcuts when problems pop up.

**Fast decisions** by the customer, based on a clear vision and a pragmatic mindset, is an inevitable factor to keep your schedule. 
If every pixel shift has be acknowledged by a project management board, you will never reach your target on time and on budget.

In the end its always people that make the difference.

## Process
Try to minimise your process and project management effort. 
**Deliver as often as you can, but at least once a week.** Establishing a [continuous delivery](http://continuousdelivery.com/) framework should be rather easy given the project is small.
<div style="float: left; padding-right: 1em;"><img height="150px" alt="market" src="hongkong99.jpg" /></div> 

Common reviews of the delivered software gives you feedback from the customer and keeps your team on track.

If your project is a fixed price project don't hassle around too much with change request management. Convince your customer to remove a not yet implemented feature each time she wants a change request to be implemented. Use a simple estimation method, e.g. story points, to find similar sized features to be traded.

## Technology
Choose a technology you know well. If you don't have any experience with [technology A](http://www.gnu.org/software/emacs/), try hard to avoid it and use the well-known [technology B](http://en.wikipedia.org/wiki/Vi) instead. 

Assure that your chosen technology allows you to deliver fast and deliver often. **It shouldn't take a week until your first code is on the production platform.** If possible, use a cloud based development environment as it allows you to quickly start delivering instead of waiting multiple days for your internal IT department to provide you with a development box.

## Fun
If you are in a motivated team, with a minimal process, delivering fast and often, there is a lot of fun conducting small software project and some of them even [changed the world](twitter.com "Twitter"). 