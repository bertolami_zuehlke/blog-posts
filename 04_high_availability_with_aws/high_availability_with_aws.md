# Building High Availability Systems in the AWS Cloud

<div style="float: left; padding-left: 1em;"><img width="220px";  src="rocinha.jpg" /></div>
*High availability may be a key feature of your software. Given every minute of downtime costs you a lot of money and trust, you might be willing to invest in a high availability infrastructure. We show you a way to build high availability systems with the Amazon Web Service (AWS) cloud*


## High availability
The [Amazon EC2 Service Level Agreement (SLA)](http://aws.amazon.com/de/ec2-sla/) promises an annual uptime percentage of at least 99.95%. This represents a maximum of 4.5h downtime every year. However, the SLA is only valid if your application runs in multiple availability zones in a region. 

The deployment to multiple availability zones within one region is quite simple. We just start at least two instances in two availability zones and add an elastic load balancer in front of them. Users access our system over the load balancer that can be created across multiple availability zones. 

<div style=""><img width="420px";  src="single_region.png" /></div>

Additionally, an auto-scaling group can be established that assures that a minimal number of instances is always running. Thus, re-creating an instance whenever an running instance is terminated.


## Higher availability

The availability can be even increase if we deploy the application to multiple regions. With the DNS fail-over feature of the Route 53 service, it becomes easy to have a backup site that is running simultaneously to your primary application in a different region around the world. Whenever the primary site is unavailable the DNS switches to the secondary site. 

<div style=""><img width="620px";  src="multi_region.png" /></div>

This all works fine as long as we run a stateless application. 
However, there is a big disclaimer when it comes to handling state that has to be available at two sites. There is no more simple solution here, because replication of state is almost always application specific. In one application it is enough to copy the data in a nightly operation from one site to the other, in another application, the data, or at least part of it has to be synchronised continuously. 

