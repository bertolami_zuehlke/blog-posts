# Big Cloud for Small Apps


*Most people associate cloud computing with large global auto-scaling applications. However, there is a different kind of applications that substantially benefit from cloud computing: small applications, like rapid prototypes or small business supporting systems.*

<div style="float: right; padding-right: 1em;"><img height="250px";  src="norwegen50_quadrat.jpg" /></div>


Running small applications in the cloud brings a number of benefits over a traditional in-house operations model. These include fast availability, new cost models and simple extensibility.

## First code to production within hours

By using cloud computing you can reduce your dependency of your internal IT department. Instead of waiting for a server to be provided by the IT department you can get one within minutes. Thus, you can typically bring your first code to production within hours. 

The set-up time of a new environment changes from days to a couple of hours or even just a couple of minutes. This can change the way you work with your customer. For example, if the customer requires an environment for presenting the system at a conference, you can just provide her a clean environment that is not used by anybody else instead of managing who is allowed to access a given environment at the specific day.


## Pay-as-you-go
Concerning cost, the big advantage for small applications is the pay-as-you-go model implemented by most cloud providers. Thus, as long as your application stays small the costs for operating your application stays small, too, increasing only when your business grows. This is a big shift away from a conventional model where a large investment in infrastructure is required upfront.

## Starting small doesn't mean you can't get big
With cloud computing, starting small doesn't mean you can't get big. When your small app becomes a big success it is relatively easy to scale your system, to increase performance, or to increase availability of the system. The success of your system is mainly defined by business success and not by technical constraints or organisational issues.


## Concerned about security?
Although cloud providers usually offer solutions to protect your sensitive data, it remains a question of trust whether you are willing to give sensitive data in others hands. However, please remember that global public cloud providers engage highly-skilled security specialists that might not be available in your internal IT department.

Fortunately, small applications often do not contain very sensitive data and no bold requirements are present concerning security. 

## Perfect fit
Although cloud computing is predestined for large, word-wide applications it is a perfect fit for small applications, too. Having an idea today, cloud computing allows you to bring it to production tomorrow.
